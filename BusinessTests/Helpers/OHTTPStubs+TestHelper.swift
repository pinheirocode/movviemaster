//
// OHTTPStubs+TestHelper.swift
// MoVViesMaster
//
// Created on 8/29/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import OHHTTPStubs

public func isRequest(_ request: URLRequest) -> OHHTTPStubsTestBlock {
  return { req in req.url?.host == request.url?.host }
}
