//
// BlockingOperator+TestHelper.swift
// MoVViesMaster
//
// Created on 8/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import RxBlocking
import RxTest
import RxSwift

extension BlockingObservable {
  func firstOrNil() -> E? {
    do {
      return try first()
    } catch {
      return nil
    }
  }

  func toArrayOrNil() -> [E]? {
    do {
      return try toArray()
    } catch {
      return nil
    }
  }

  func error() -> Error? {
    // When
    do {
      _ = try self.first()
      return nil
    } catch {
      return error
    }
  }

  func error<T>() -> T? {
    // When
    do {
      _ = try self.first()
      return nil
    } catch {
      return error as? T
    }
  }

  func throwsError<T: Error>(_ error: T.Type) -> Bool {
    // When
    do {
      _ = try self.first()
      return false
    } catch _ as T {
      return true
    } catch {
      return false
    }
  }
}

extension TestableObserver {
  func eventElements() -> [ElementType] {
    return events.map { $0.value.element! }
  }
}
