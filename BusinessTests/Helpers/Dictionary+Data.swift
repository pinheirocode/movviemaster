//
// Dictionary+Data.swift
// MoVViesMaster
//
// Created on 8/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

extension Dictionary {
  var asData: Data {
    return try! JSONSerialization.data(withJSONObject: self)
  }
}
