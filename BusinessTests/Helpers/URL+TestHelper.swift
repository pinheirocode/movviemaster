//
// URL+TestHelper.swift
// MoVViesMaster
//
// Created on 7/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

extension URL {

  var queryItemsDictionary: [String: String] {
    let urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: false)
    let authParams = urlComponents!.queryItems!
      .reduce([String: String]()) { (accumulator: [String: String], item: URLQueryItem) -> [String: String] in
      var result = accumulator
      result[item.name] = item.value ?? ""
      return result
    }
    return authParams
  }
}
