//
// MockMoviesProvider.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import Business

class MockMoviesProvider: MovieProvider {

  var invokedFetchMovies = false
  var invokedFetchMoviesCount = 0
  var invokedFetchMoviesParameters: (page: Int, query: String)?
  var invokedFetchMoviesParametersList = [(page: Int, query: String)]()
  var stubbedFetchMoviesCompletionResult: ([Movie]?, ProviderError?)?

  func fetchMovies(page: Int, query: String, completion: @escaping FetchResultHandler) {
    invokedFetchMovies = true
    invokedFetchMoviesCount += 1
    invokedFetchMoviesParameters = (page, query)
    invokedFetchMoviesParametersList.append((page, query))
    if let result = stubbedFetchMoviesCompletionResult { completion(result.0, result.1) }
  }

  var invokedFetchUpcoming = false
  var invokedFetchUpcomingCount = 0
  var invokedFetchUpcomingParameters: (page: Int, Void)?
  var invokedFetchUpcomingParametersList = [(page: Int, Void)]()
  var stubbedFetchUpcomingCompletionResult: ([Movie]?, ProviderError?)?

  func fetchUpcoming(page: Int, completion: @escaping FetchResultHandler) {
    invokedFetchUpcoming = true
    invokedFetchUpcomingCount += 1
    invokedFetchUpcomingParameters = (page, ())
    invokedFetchUpcomingParametersList.append((page, ()))
    if let result = stubbedFetchUpcomingCompletionResult { completion(result.0, result.1) }
  }

  // MARK: - Backwards Compatibility

  var upcomingMovies: [Movie] { didSet { stubbedFetchUpcomingCompletionResult = (upcomingMovies, nil) }}
  var filteredMovies: [Movie] { didSet { stubbedFetchMoviesCompletionResult = (filteredMovies, nil) }}

  var receivedQuery: String? { get { return invokedFetchMoviesParameters?.query } }
  var requestedPageForUpcoming: Int { get { return invokedFetchUpcomingParameters?.page ?? -1 } }
  var requestedPageForFiltered: Int { get { return invokedFetchMoviesParameters?.page ?? -1 } }

  convenience init() {
    self.init(upcomingMovies: [], filteredMovies: [])
  }

  init(upcomingMovies: [Movie], filteredMovies: [Movie]) {
    self.upcomingMovies = upcomingMovies
    self.filteredMovies = filteredMovies
    stubbedFetchUpcomingCompletionResult = (upcomingMovies, nil)
    stubbedFetchMoviesCompletionResult = (filteredMovies, nil)
  }
}
