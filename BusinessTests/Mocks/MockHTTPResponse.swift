//
// MockHTTPResponse.swift
// MoVViesMaster
//
// Created on 7/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

class MockHTTPResponse: HTTPURLResponse {
  init(url: URL = URL(string: "https://mock.url")!, status: Int = 200, headerFields: [String: String]? = nil) {
    super.init(url: url, statusCode: status, httpVersion: nil, headerFields: headerFields)!
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
