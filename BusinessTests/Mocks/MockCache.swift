//
// MockCache.swift
// MoVViesMaster
//
// Created on 7/31/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import UIKit

final class MockCache: NSCache<NSString, UIImage> {
  override var name: String {
    get {
      return super.name
    }
    set {
      super.name = newValue
    }
  }

  override var delegate: NSCacheDelegate? {
    get {
      return super.delegate
    }
    set {
      super.delegate = newValue
    }
  }

  var data: [String: UIImage]
  var requestedKey: String?

  init(seed: [String: UIImage]) {
    self.data = seed
    super.init()
  }

  override func object(forKey key: NSString) -> UIImage? {
    self.requestedKey = key as String
    return data[key as String]
  }

  override func setObject(_ obj: UIImage, forKey key: NSString) {
    data[key as String] = obj
  }

  override func setObject(_ obj: UIImage, forKey key: NSString, cost g: Int) {
    data[key as String] = obj
  }

  override func removeObject(forKey key: NSString) {
    data[key as String] = nil
  }

  override func removeAllObjects() {
    data.removeAll()
  }
}
