//
// MockUrlSession.swift
// MoVViesMaster
//
// Created on 7/27/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

class MockDataTask: URLSessionDataTask {
  var responseData: Data?
  var urlResponse: URLResponse?
  var responseError: Error?
  var completionHandler: (Data?, URLResponse?, Error?) -> Void
  var executed = false

  init(
    responseData: Data? = nil,
    urlResponse: URLResponse? = MockHTTPResponse(),
    responseError: Error? = nil,
    handler: @escaping (Data?, URLResponse?, Error?) -> Void = { _ in return }) {

    self.responseData = responseData
    self.urlResponse = urlResponse
    self.responseError = responseError
    self.completionHandler = handler

    super.init()
  }

  override func resume() {
    executed = true
    completionHandler(responseData, urlResponse, responseError)
  }

  override func cancel() { }
}

class MockDownloadDataTask: URLSessionDownloadTask {
  var resultFileUrl: URL?
  var urlResponse: URLResponse?
  var responseError: Error?
  var completionHandler: (URL?, URLResponse?, Error?) -> Void
  var executed = false

  init(
    resultFileUrl: URL? = nil,
    urlResponse: URLResponse? = MockHTTPResponse(),
    responseError: Error? = nil,
    handler: @escaping (URL?, URLResponse?, Error?) -> Void = { _ in return }) {

    self.resultFileUrl = resultFileUrl
    self.urlResponse = urlResponse
    self.responseError = responseError
    self.completionHandler = handler

    super.init()
  }

  override func resume() {
    executed = true
    completionHandler(resultFileUrl, urlResponse, responseError)
  }

  override func cancel() { }
}

class MockUrlSession: URLSession {
  var requestUrl: String?
  var mockDataTask = MockDataTask()
  var receivedRequest: URLRequest?
  var responseData: Data?

  var mockDownloadDataTask = MockDownloadDataTask()
  var receivedDownloadRequest: URLRequest?
  var fileUrl: URL?

  override init() {
  }

  override func dataTask(with request: URLRequest) -> URLSessionDataTask {
    receivedRequest = request
    return MockDataTask { _ in return }
  }

  override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
    receivedRequest = request
    mockDataTask.completionHandler = completionHandler
    return mockDataTask
  }

  override func dataTask(with url: URL) -> URLSessionDataTask {
    fatalError()
  }

  override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
    fatalError()
  }

  override func downloadTask(with request: URLRequest) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(with url: URL) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(withResumeData resumeData: Data) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(with request: URLRequest, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) -> URLSessionDownloadTask {
    receivedDownloadRequest = request
    mockDownloadDataTask.completionHandler = completionHandler
    return mockDownloadDataTask
  }

  override func downloadTask(with url: URL, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) -> URLSessionDownloadTask {
    fatalError()
  }

  override func downloadTask(withResumeData resumeData: Data, completionHandler: @escaping (URL?, URLResponse?, Error?) -> Void) -> URLSessionDownloadTask {
    fatalError()
  }
}
