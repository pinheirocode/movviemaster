//
// Stub.swift
// MoVViesMaster
//
// Created on 7/27/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

class Stub {
  static func jsonData(named name: String) -> Data {
    return fileData(named: name, withExtension: "json")
  }

  static func imageData(named name: String) -> Data {
    return fileData(named: name, withExtension: "jpg")
  }

  static func fileData(named name: String, withExtension ext: String) -> Data {
    return try! Data(contentsOf: fileUrl(named: name, withExtension: ext))
  }

  static func imageUrl(named name: String, withExtension ext: String = "jpg") -> URL {
    return fileUrl(named: name, withExtension: ext)
  }

  static func fileUrl(named name: String, withExtension ext: String) -> URL {
    let bundle = Bundle(for: self)
    return bundle.url(forResource: name, withExtension: ext)!
  }
}
