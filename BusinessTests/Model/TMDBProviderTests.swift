//
// TMDBProviderTests.swift
// MoVViesMaster
//
// Created on 7/27/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
import Foundation
@testable import Business

class TMDBProviderTests: XCTestCase {
  fileprivate var mockUrlSession: MockUrlSession!

  override func setUp() {
    super.setUp()
    mockUrlSession = MockUrlSession()
  }

  override func tearDown() {
    super.tearDown()
  }

}

extension TMDBProviderTests {

  // MARK: - Upcoming movies

  func testFetchUpcoming_CreatesRequestUsingUpcomingMoviesUrl() {
    // Given
    let provider = TMDBProvider(urlSession: mockUrlSession)

    // When
    provider.fetchUpcoming { _ in return }

    // Then
    XCTAssertTrue(mockUrlSession.receivedRequest!.url!.absoluteString.hasPrefix(TMDB.upcomingMovies.url.absoluteString))
  }

  func testFetchUpcoming_Query_RequiredParametersAreAdded() {
    // Given
    let provider = TMDBProvider(urlSession: mockUrlSession)

    // When
    provider.fetchUpcoming { _ in return }

    // Then
    let queryParams = mockUrlSession.receivedRequest!.url!.queryItemsDictionary

    XCTAssertNotNil(queryParams["api_key"], "Parameter `api_key` not found")
    XCTAssertNotNil(queryParams["page"], "Parameter `page` not found")
    XCTAssertNotNil(queryParams["language"], "Parameter `language` not found")
  }

  func testFetchUpcoming_Pagination_SetCorrectParameter() {
    // Given
    let provider = TMDBProvider(urlSession: mockUrlSession)

    // When
    provider.fetchUpcoming(page: 1) { _ in return }

    // Then
    var queryParams = mockUrlSession.receivedRequest!.url!.queryItemsDictionary
    XCTAssertEqual(queryParams["page"], "1")

    // When
    provider.fetchUpcoming(page: 2) { _ in return }

    // Then
    queryParams = mockUrlSession.receivedRequest!.url!.queryItemsDictionary
    XCTAssertEqual(queryParams["page"], "2")
  }

  // MARK: - Success

  func testFetchUpcoming_Success_CompletionReceivesParsedObjects() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.jsonData(named: "UpcomingMovies_20"))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedMovies: [Movie]? = nil

    // When
    provider.fetchUpcoming { movies, _ in
      receivedMovies = movies
    }

    // Then
    XCTAssertNotNil(receivedMovies)
    XCTAssertEqual(receivedMovies!.count, 20)
  }

  func testFetchUpcoming_Success_EmptyMoviesList_CompletionReceivesEmptyMoviesArray() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.jsonData(named: "UpcomingMovies_Empty"))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedMovies: [Movie]? = nil

    // When
    provider.fetchUpcoming { movies, _ in
      receivedMovies = movies
    }

    // Then
    XCTAssertNotNil(receivedMovies)
    XCTAssertTrue(receivedMovies!.isEmpty)
  }

  func testFetchUpcoming_Success_MissingResponseData_CompletionReceivesEmptyMoviesArray() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask()
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedMovies: [Movie]? = nil

    // When
    provider.fetchUpcoming { movies, _ in
      receivedMovies = movies
    }

    // Then
    guard let unwrappedMovies = receivedMovies else {
      XCTFail("Expected [Movie] object not found. Expected empty array of Movie objects.")
      return
    }
    XCTAssertTrue(unwrappedMovies.isEmpty)
  }

  // MARK: - Failure

  func testFetchUpcoming_FailWithError_CompletionReceivesRequestErrorObjects() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseError: NSError(domain: "Test", code: -1))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Any? = nil

    // When
    provider.fetchUpcoming { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.unknown(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.unknown(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }

  func testFetchUpcoming_FailWithInvalidResponseData_CompletionReceivesCorrectError() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.jsonData(named: "InvalidJson"))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Error? = nil

    // When
    provider.fetchUpcoming { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.invalidResponseData(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.invalidResponseData(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }

  func testFetchUpcoming_FailWithStatus_401_CompletionReceivesRequestErrorObjects() {
    // Given
    let mockUrlResponse = MockHTTPResponse(status: 401)
    mockUrlSession.mockDataTask = MockDataTask(urlResponse: mockUrlResponse)
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Any? = nil

    // When
    provider.fetchUpcoming { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.accessDenied(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.accessDenied(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }

  // MARK: - Filtered Movies

  func testFetchMovies_Query_CreatesRequestUsingFilteredMoviesUrl() {
    // Given
    let provider = TMDBProvider(urlSession: mockUrlSession)

    // When
    let query = "valid"
    provider.fetchMovies(query: query) { _ in return }

    // Then
    let searchUrl = TMDB.searchMovies.url.absoluteString
    XCTAssertTrue(mockUrlSession.receivedRequest!.url!.absoluteString.hasPrefix(searchUrl))
  }

  func testFetchMovies_Query_RequiredParametersAreAdded() {
    // Given
    let provider = TMDBProvider(urlSession: mockUrlSession)

    // When
    let query = "valid"
    provider.fetchMovies(query: query) { _ in return }

    // Then
    let queryParams = mockUrlSession.receivedRequest!.url!.queryItemsDictionary

    XCTAssertNotNil(queryParams["query"], "Parameter `query` not found")
    XCTAssertEqual(queryParams["query"], query)
    XCTAssertNotNil(queryParams["api_key"], "Parameter `api_key` not found")
    XCTAssertNotNil(queryParams["page"], "Parameter `page` not found")
    XCTAssertNotNil(queryParams["language"], "Parameter `language` not found")
  }

  func testFetchMovies_Pagination_SetCorrectParameter() {
    // Given
    let provider = TMDBProvider(urlSession: mockUrlSession)

    // When
    provider.fetchMovies(page: 1, query: "valid") { _ in return }

    // Then
    var queryParams = mockUrlSession.receivedRequest!.url!.queryItemsDictionary
    XCTAssertEqual(queryParams["page"], "1")

    // When
    provider.fetchMovies(page: 2, query: "valid") { _ in return }

    // Then
    queryParams = mockUrlSession.receivedRequest!.url!.queryItemsDictionary
    XCTAssertEqual(queryParams["page"], "2")
  }

  // MARK: - Success

  func testFetchMovies_Success_CompletionReceivesParsedObjects() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.jsonData(named: "FilteredMovies_20"))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedMovies: [Movie]? = nil

    // When
    provider.fetchMovies(query: "valid") { movies, _ in
      receivedMovies = movies
    }

    // Then
    XCTAssertNotNil(receivedMovies)
    XCTAssertEqual(receivedMovies!.count, 20)
  }

  func testFetchMovies_Success_EmptyMoviesList_CompletionReceivesEmptyMoviesArray() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.jsonData(named: "FilteredMovies_Empty"))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedMovies: [Movie]? = nil

    // When
    provider.fetchMovies(query: "valid") { movies, _ in
      receivedMovies = movies
    }

    // Then
    XCTAssertNotNil(receivedMovies)
    XCTAssertTrue(receivedMovies!.isEmpty)
  }

  func testFetchMovies_Success_MissingResponseData_CompletionReceivesEmptyMoviesArray() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask()
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedMovies: [Movie]? = nil

    // When
    provider.fetchMovies(query: "valid") { movies, _ in
      receivedMovies = movies
    }

    // Then
    guard let unwrappedMovies = receivedMovies else {
      XCTFail("Expected [Movie] object not found. Expected empty array of Movie objects.")
      return
    }
    XCTAssertTrue(unwrappedMovies.isEmpty)
  }

  // MARK: - Failure

  func testFetchMovies_FailWithError_CompletionReceivesRequestErrorObjects() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseError: NSError(domain: "Test", code: -1))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Any? = nil

    // When
    provider.fetchMovies(query: "valid") { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.unknown(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.unknown(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }

  func testFetchMovies_FailWithInvalidResponseData_CompletionReceivesCorrectError() {
    // Given
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.jsonData(named: "InvalidJson"))
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Error? = nil

    // When
    provider.fetchMovies(query: "valid") { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.invalidResponseData(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.invalidResponseData(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }

  func testFetchMovies_FailWithStatus_401_CompletionReceivesRequestErrorObjects() {
    // Given
    let mockUrlResponse = MockHTTPResponse(status: 401)
    mockUrlSession.mockDataTask = MockDataTask(urlResponse: mockUrlResponse)
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Any? = nil

    // When
    provider.fetchMovies(query: "valid") { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.accessDenied(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.accessDenied(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }

  func testFetchMovies_EmptyQuery_CompletionReceivesRequestErrorObjects() {
    // Given
    let mockUrlResponse = MockHTTPResponse(status: 401)
    mockUrlSession.mockDataTask = MockDataTask(urlResponse: mockUrlResponse)
    let provider = TMDBProvider(urlSession: mockUrlSession)
    var receivedError: Any? = nil

    // When
    provider.fetchMovies(query: "") { _, error in
      receivedError = error
    }

    // Then
    guard let unwrappedError = receivedError else {
      XCTFail("No error object was received")
      return
    }
    guard let _ = unwrappedError as? ProviderError,
          case ProviderError.invalidRequestData(_) = unwrappedError else {
      XCTFail("Wrong error was received. " +
                "Expected: \(ProviderError.invalidRequestData(message: "Any message")) " +
                "Found: \(unwrappedError)")
      return
    }
  }
}
