//
// MovieTests.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
@testable import Business

final class MovieTests: XCTestCase {
  var movie: Movie!
  var mockDateString: String!
  var mockDate: Date!

  override func setUp() {
    super.setUp()
    movie = Movie()
    mockDateString = "2005-12-14"

    let formatter = DateFormatter()
    formatter.dateFormat = "yy-MM-dd"
    mockDate = formatter.date(from: mockDateString)
  }

  override func tearDown() {
    movie = nil
    mockDateString = nil
    mockDate = nil
    super.tearDown()
  }

  func testInitWithDictionary_ShouldPopulateAllFields() {
    // Given
    let stub: [String: Any] = [
      "vote_count": 2086,
      "id": 254,
      "video": false,
      "vote_average": 6.6,
      "title": "King Kong",
      "popularity": 6.232044,
      "poster_path": "/iQYMVoI9QE2wQCOSrxhiBYd4v0w.jpg",
      "original_language": "en",
      "original_title": "King Kong",
      "genre_ids": [12, 18, 28],
      "backdrop_path": "/7FIh52gySnFC11HluVcaPkBjPit.jpg",
      "adult": false,
      "overview": "In 1933 New York, an overly ambitious movie producer coerces his cast and hired ship crew to travel to mysterious Skull Island, where they encounter Kong, a giant ape who is immediately smitten with the leading lady.",
      "release_date": mockDateString
    ]

    // When
    let movie = Movie(with: stub)

    // Then
    XCTAssertEqual(movie.voteCount, 2086)
    XCTAssertEqual(movie.id, 254)
    XCTAssertEqual(movie.video, false)
    XCTAssertEqual(movie.voteAverage, 6.6)
    XCTAssertEqual(movie.title, "King Kong")
    XCTAssertEqual(movie.popularity, 6.232044)
    XCTAssertEqual(movie.posterPath, "/iQYMVoI9QE2wQCOSrxhiBYd4v0w.jpg")
    XCTAssertEqual(movie.originalLanguage, "en")
    XCTAssertEqual(movie.originalTitle, "King Kong")
    XCTAssertEqual(movie.genreIds!, [12, 18, 28 ])
    XCTAssertEqual(movie.backdropPath, "/7FIh52gySnFC11HluVcaPkBjPit.jpg")
    XCTAssertEqual(movie.adult, false)
    XCTAssertEqual(movie.overview, "In 1933 New York, an overly ambitious movie producer coerces his cast and hired ship crew to travel to mysterious Skull Island, where they encounter Kong, a giant ape who is immediately smitten with the leading lady.")
    XCTAssertEqual(movie.releaseDate, mockDate)
  }

  func testParseDate_ReturnsCorrectDateObject() {
    // When
    let date = movie.parseDate("2005-12-14")

    // Then
    XCTAssertEqual(date, mockDate)
  }
}
