//
// MoviesProviderRxTests.swift
// MoVViesMaster
//
// Created on 8/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxTest
import RxBlocking
import Nimble
@testable import Business

final class MoviesProviderRxTests: XCTestCase {
  var mockProvider: MockMoviesProvider!

  let mockUpcomingMovies = [
    Movie(with: ["id": "SuperID", "title": "Superman"]),
    Movie(with: ["id": "XID", "title": "X-Man"])
  ]

  let mockFilteredMovies = [
    Movie(with: ["id": "WonderID", "title": "Wonder Woman"]),
    Movie(with: ["id": "FlashID", "title": "The Flesh"]),
    Movie(with: ["id": "DD-ID", "title": "Dare Devil"])
  ]

  override func setUp() {
    super.setUp()
    mockProvider = MockMoviesProvider()
  }

  override func tearDown() { super.tearDown() }

  func testFetch_SuccessRequest_SpawnsMoviesEvents() {
    // Given
    mockProvider.stubbedFetchMoviesCompletionResult = (mockFilteredMovies, nil)
    let observable = mockProvider.rx.fetchMovies(page: 0, query: "valid")

    // When
    let result = observable.toBlocking(timeout: 1).firstOrNil()

    // Then
    expect(result) == mockFilteredMovies
  }

  func testFetch_FailureRequest_SpawnsError() {
    // Given
    mockProvider.stubbedFetchMoviesCompletionResult = (nil, ProviderError.unknown(message: "Intentionally failing"))
    let observable = mockProvider.rx.fetchMovies(page: 0, query: "valid")

    // When
    let receivedError = observable.toBlocking().error()

    // Then
    expect(receivedError).to(beAnInstanceOf(ProviderError.self))
  }
}
