//
// MoviesListViewModelTests.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
import RxSwift
import RxTest
import Nimble
import RxBlocking
@testable import Business

final class MoviesListViewModelTests: XCTestCase {
  var viewModel: MoviesListViewModel!
  var scheduler: TestScheduler!
  var subscription: RxSwift.Disposable!
  var disposeBag: DisposeBag!

  fileprivate let mockUpcomingMovies = [
    Movie(with: ["id": "SuperID", "title": "Superman"]),
    Movie(with: ["id": "XID", "title": "X-Man"])
  ]

  fileprivate let mockFilteredMovies = [
    Movie(with: ["id": "WonderID", "title": "Wonder Woman"]),
    Movie(with: ["id": "FlashID", "title": "The Flesh"]),
    Movie(with: ["id": "DD-ID", "title": "Dare Devil"])
  ]

  fileprivate var mockMoviesProvider: MockMoviesProvider!

  override func setUp() {
    super.setUp()
    scheduler = TestScheduler(initialClock: 0)
    disposeBag = DisposeBag()

    mockMoviesProvider = MockMoviesProvider(upcomingMovies: mockUpcomingMovies, filteredMovies: mockFilteredMovies)
    viewModel = MoviesListViewModel(provider: mockMoviesProvider)
    viewModel.query.value = ""
  }

  override func tearDown() {
    viewModel = nil
    scheduler.scheduleAt(1000) {
      self.subscription.dispose()
    }

    disposeBag = nil

    super.tearDown()
  }

  // MARK: - Upcoming + Search

  func testLoadFiltered_EmptyQuery_ShowsUpcoming() {
    // When
    viewModel.loadMovies()

    // Then
    XCTAssertTrue(mockMoviesProvider.invokedFetchUpcoming)
  }

  // MARK: - Upcoming

  func testLoadUpcoming_UpdatesWorkingStatusProperty() {
    // Given
    let observer = scheduler.createObserver(Bool.self)
    viewModel.working.asObservable().subscribe(observer).disposed(by: disposeBag)

    // When
    viewModel.loadMovies()

    // Then
    expect(observer.eventElements()) == [false, true, false]
  }

  func testLoadUpcoming_PopulatesMoviesPropertyWithProviderResult() {
    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockUpcomingMovies)
  }

  func testLoadUpcoming_CallsIncreaseNextPageNumber() {
    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.nextPageForUpcoming.value, 2)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.nextPageForUpcoming.value, 3)
  }

  func testLoadUpcoming_CallsPassCorrectPageNumber() {
    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(mockMoviesProvider.requestedPageForUpcoming, 1)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(mockMoviesProvider.requestedPageForUpcoming, 2)
  }

  func testLoadUpcoming_AddNewMoviesToExistingList() {
    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value.count, 2)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value.count, 4)
  }

  // MARK: - Search

  func testLoadSearchResults_UsesTheValueOfQueryProperty() {
    // Given
    let mockQuery = "Query"
    viewModel.query.value = mockQuery

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(mockMoviesProvider.receivedQuery, mockQuery)
  }

  func testLoadSearchResults_ValidQuery_FetchesMoviesWithQuery() {
    // Given
    let mockValidQuery = "valid"
    viewModel.query.value = mockValidQuery

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertTrue(mockMoviesProvider.invokedFetchMovies)
    XCTAssertEqual(mockMoviesProvider.receivedQuery, mockValidQuery)
  }

  func testLoadSearchResults_ValidQuery_UpdatesWorkingStatusProperty() {
    // Given
    let observer = scheduler.createObserver(Bool.self)
    viewModel.working.asObservable().subscribe(observer).disposed(by: disposeBag)

    // When
    viewModel.loadMovies()

    // Then
    expect(observer.eventElements()) == [false, true, false]
  }

  func testSearchAction_LoadSearchResults_ValidQuery_ChangesActionToClose() {
    // Given
    let observer = scheduler.createObserver(MovieListAction.self)
    viewModel.query.value = "valid"
    viewModel.action.asObservable().subscribe(observer).disposed(by: disposeBag)

    // When
    viewModel.loadMovies()

    // Then
    expect(observer.eventElements()) == [MovieListAction.search, MovieListAction.close]
  }

  func testSearchAction_MultipleLoadSearchResults_doNOT_ChangesActionToSearch() {
    // Given
    let observer = scheduler.createObserver(MovieListAction.self)
    viewModel.query.value = "valid"
    viewModel.action.asObservable().subscribe(observer).disposed(by: disposeBag)

    // When
    viewModel.loadMovies()
    viewModel.loadMovies()

    // Then
    expect(observer.eventElements()) == [MovieListAction.search, MovieListAction.close, MovieListAction.close]
  }

  func testSearchAction_ChangeQuery_ChangesActionToSearch() {
    // Given
    let observer = scheduler.createObserver(MovieListAction.self)
    viewModel.query.value = "valid"
    viewModel.action.asObservable().subscribe(observer).disposed(by: disposeBag)
    viewModel.loadMovies()

    // When
    viewModel.query.value = "new value"

    // Then
    expect(observer.eventElements()) == [MovieListAction.search, MovieListAction.close, MovieListAction.search]
  }

  func testLoadSearchResults_ValidQuery_PopulatesMoviesPropertyWithProviderResult() {
    // Given
    viewModel.query.value = "valid"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockFilteredMovies)
  }

  func testLoadSearchResults_ValidQuery_CallsIncreaseNextPageNumber() {
    // Given
    viewModel.query.value = "valid"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.nextPageForFiltered.value, 2)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.nextPageForFiltered.value, 3)
  }

  func testLoadFiltered_CallsPassCorrectPageNumber() {
    // Given
    viewModel.query.value = "valid"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(mockMoviesProvider.requestedPageForFiltered, 1)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(mockMoviesProvider.requestedPageForFiltered, 2)
  }

  func testLoadFiltered_ValidQuery_AddNewMoviesToExistingList() {
    // Given
    viewModel.query.value = "valid"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value.count, 3)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value.count, 6)
  }

  func testLoadFiltered_ChangeQuery_ResetsPageCount() {
    // Given
    viewModel.query.value = "valid"
    viewModel.loadMovies()

    // When
    viewModel.query.value = "new query"

    // Then
    XCTAssertEqual(viewModel.nextPageForFiltered.value, 1)
  }

  func testLoadFiltered_ChangeQuery_CallsIncrementPageCount() {
    // Given
    viewModel.query.value = "valid"
    viewModel.loadMovies()
    viewModel.query.value = "new query"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.nextPageForFiltered.value, 2)

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.nextPageForFiltered.value, 3)
  }

  func testLoadFiltered_ChangeQuery_DoesNotClearMovies() {
    // Given
    viewModel.query.value = "valid"
    viewModel.loadMovies()
    let countBefore = viewModel.movies.value.count

    // When
    viewModel.query.value = "new query"

    // Then
    let countAfter = viewModel.movies.value.count
    XCTAssertEqual(countBefore, countAfter)
  }

  func testLoadFiltered_AFTER_ChangeQueryToValidQuery_ClearsPreviousFilteredData() {
    // Given
    viewModel.query.value = "valid"
    viewModel.loadMovies()
    viewModel.query.value = "new query"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockFilteredMovies)
  }

  // MARK: - Alternating Upcoming <-> Filtered

  func testLoadFiltered_AFTER_LoadUpcoming_ReturnsListOfFiltered() {
    // Given
    viewModel.loadMovies()
    viewModel.query.value = "valid"

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockFilteredMovies)
  }

  func testLoadUpcoming_AFTER_LoadFiltered_ReturnsListOfUpcoming() {
    // Given
    viewModel.query.value = "valid"
    viewModel.loadMovies()
    viewModel.query.value = ""

    // When
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockUpcomingMovies)
  }

  func testLoadUpcoming_MultipleTimes_AFTER_LoadFiltered_AddsNewMoviesToExistingList() {
    // Given
    viewModel.query.value = "valid"
    viewModel.loadMovies()
    viewModel.query.value = ""

    // When
    viewModel.loadMovies()
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockUpcomingMovies + mockUpcomingMovies)
  }

  func testSwitch_Upcoming_Filtered_UpcomingAgain_SpawnsUpdateEventForEmptyList() {
    // Given
    let observer = scheduler.createObserver(MoviesUpdate.self)
    viewModel.loadMovies()
    viewModel.query.value = "car"
    viewModel.loadMovies()

    // When
    self.subscription = viewModel.listUpdates.asObservable().subscribe(observer)
    viewModel.query.value = ""
    viewModel.loadMovies()

    // Then
    let events = observer.eventElements()
    expect(events.count) == 1
    guard let event = events.first, case MoviesUpdate.refreshMovies = event else {
      fail("Wrong event list. Expected \([MoviesUpdate.refreshMovies]); Found: \(events)")
      return
    }
  }

  func testLoadFiltered_MultipleTimes_AFTER_LoadUpcoming_AddsNewMoviesToExistingList() {
    // Given
    viewModel.loadMovies()
    viewModel.query.value = "valid"

    // When
    viewModel.loadMovies()
    viewModel.loadMovies()

    // Then
    XCTAssertEqual(viewModel.movies.value, mockFilteredMovies + mockFilteredMovies)
  }

  // MARK: - Item Selection

  func testSelectionWithValidIndex_ShouldPopulateSelectedMovie() {
    // Given
    viewModel.loadMovies()
    let selectedIndex = IndexPath(row: 0, section: 0)

    // When
    viewModel.didSelect(index: selectedIndex)

    // Then
    XCTAssertEqual(viewModel.selectedMovie, mockUpcomingMovies[0])
  }
}
