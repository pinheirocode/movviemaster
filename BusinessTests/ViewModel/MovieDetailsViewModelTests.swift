//
// MovieDetailsViewModelTests.swift
// MoVViesMaster
//
// Created on 8/4/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
@testable import Business

class MovieDetailsViewModelTests: XCTestCase {
  var mockMovie: Movie!
  override func setUp() {
    super.setUp()
    mockMovie = Movie(with: [
      "id": "SuperID",
      "title": "Superman",
      "overview": "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond."
    ])
  }

  override func tearDown() {
    super.tearDown()
    mockMovie = nil
  }

  func testInitWithMovies_PopulatesProperties() {
    // When
    let viewModel = MovieDetailViewModel(with: mockMovie)

    // Then
    XCTAssertEqual(viewModel.title.value, mockMovie.title)
    XCTAssertEqual(viewModel.overview.value, mockMovie.overview)
  }
}
