//
// DynamicPropertyTests.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
@testable import Business

class DynamicPropertyTests: XCTestCase {
  let mockStringValue = "Mock Value"

  override class func setUp() {
    super.setUp()
  }

  override class func tearDown() {
    super.tearDown()
  }

  // MARK: - Initialization and assignment

  func testStoresInitialValue() {
    // Given
    let expectedValue = "Initial Value"
    let dynamicProperty = DynamicProperty<String>(expectedValue)

    // When
    let storedValue = dynamicProperty.value

    // Then
    XCTAssertEqual(storedValue, expectedValue)
  }

  func testNullableType_AcceptsNilAsInitialValue() {
    // Given
    let dynamicProperty = DynamicProperty<String?>(nil)

    // When
    let storedValue = dynamicProperty.value

    // Then
    XCTAssertNil(storedValue)
  }

  func testAssignValue_StoresNewValue() {
    // Given
    let initialValue = "Initial Value"
    let dynamicProperty = DynamicProperty<String>(initialValue)
    let newValue = "New Value"

    // When
    dynamicProperty.value = newValue

    // Then
    XCTAssertEqual(dynamicProperty.value, newValue)
  }

  func testNullableType_StoresNewValue() {
    // Given
    let dynamicProperty = DynamicProperty<String?>(nil)
    let newValue = "New Value"

    // When
    dynamicProperty.value = newValue

    // Then
    XCTAssertEqual(dynamicProperty.value, newValue)
  }

  // MARK: - Event dispatching

  func testWillChange_HandlerIsCalledWhenValueIsAssigned() {
    // Given
    let dynamicProperty = DynamicProperty<String>(mockStringValue)
    var eventHandlerCalled = false
    _ = dynamicProperty.willChange.add { _, _ in eventHandlerCalled = true }

    // When
    dynamicProperty.value = "New Value"

    // Then
    XCTAssert(eventHandlerCalled, "Event handler was not called")
  }

  func testWillChange_HandlerIsGivenCurrentAndNewValues() {
    // Given
    let dynamicProperty = DynamicProperty<String>(mockStringValue)
    var valueOfWillChangeEvent: (String, String)? = nil
    _ = dynamicProperty.willChange.add { eventValue in valueOfWillChangeEvent = eventValue }
    let mockNewValue = "New Value"

    // When
    dynamicProperty.value = mockNewValue

    // Then
    guard let (current, new) = valueOfWillChangeEvent else {
      XCTFail("Current and new values were not properly populated.")
      return
    }

    XCTAssertEqual(current, mockStringValue)
    XCTAssertEqual(new, mockNewValue)
  }

  func testDidChange_HandlerIsCalledWhenValueIsAssigned() {
    // Given
    let dynamicProperty = DynamicProperty<String>(mockStringValue)
    var eventHandlerCalled = false
    _ = dynamicProperty.didChange.add { _, _ in eventHandlerCalled = true }

    // When
    dynamicProperty.value = "New Value"

    // Then
    XCTAssert(eventHandlerCalled, "Event handler was not called")
  }

  func testDidChange_HandlerIsGivenCurrentAndNewValues() {
    // Given
    let dynamicProperty = DynamicProperty<String>(mockStringValue)
    var valueOfDidChangeEvent: (String, String)? = nil
    _ = dynamicProperty.didChange.add { eventValue in valueOfDidChangeEvent = eventValue }
    let mockNewValue = "New Value"

    // When
    dynamicProperty.value = mockNewValue

    // Then
    guard let (old, current) = valueOfDidChangeEvent else {
      XCTFail("Current and new values were not properly populated.")
      return
    }

    XCTAssertEqual(old, mockStringValue)
    XCTAssertEqual(current, mockNewValue)
  }

  func testLatestValue_HandlerIsGivenInitialValueUponSubscription() {
    // Given
    let dynamicProperty = DynamicProperty<Int>(0)
    var currentValue = -1

    // When
    _ = dynamicProperty.latestValue.add { value in
      currentValue = value
    }

    // Then
    XCTAssertEqual(currentValue, 0)
  }

  func testLatestValue_HandlerIsGivenCurrentValueUponSubscription() {
    // Given
    let dynamicProperty = DynamicProperty<Int>(0)
    var currentValue = -1
    dynamicProperty.value = 1

    // When
    _ = dynamicProperty.latestValue.add { value in
      currentValue = value
    }

    // Then
    XCTAssertEqual(currentValue, 1)
  }
}
