//
// ImageStoreTests.swift
// MoVViesMaster
//
// Created on 7/31/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
import RxBlocking
import Nimble
@testable import Business

class ImageStoreTests: XCTestCase {
  var mockImagePath: String!
  var mockImage: UIImage!
  var mockCache: MockCache!
  var mockUrlSession: MockUrlSession!
  var store: ImageStore!

  override func setUp() {
    super.setUp()
    mockImagePath = "mock.image.jpg"
    mockImage = UIImage()
    mockCache = MockCache(seed: [mockImagePath: mockImage])
    mockUrlSession = MockUrlSession()
    store = ImageStore(cache: mockCache)
  }

  override func tearDown() {
    super.tearDown()
  }

  func testImageFromUrl_RetrievesImageFromCache() {
    // Given
    let mockImageUrlString = TMDB.image.w92(path: mockImagePath).url.absoluteString
    mockCache = MockCache(seed: [mockImageUrlString: mockImage])
    store = ImageStore(cache: mockCache)

    // When
    let receivedImage = store.image(.w92(path: mockImagePath)).toBlocking().firstOrNil()

    // Then
    XCTAssertEqual(receivedImage, mockImage)
  }

  func testImageFromUrl_NoCacheHit_DownloadsImage() {
    // Given
    mockCache = MockCache(seed: [:])
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.imageData(named: "SampleImage"))
    store = ImageStore(cache: mockCache, urlSession: mockUrlSession)

    // When
    let pathNotInCache = "path/not/in/cache.jpg"
    _ = store.image(.w92(path: pathNotInCache)).toBlocking(timeout: 1).firstOrNil()

    // Then
    let imageUrl = TMDB.image.w92(path: pathNotInCache).url.absoluteString
    expect(self.mockCache.requestedKey) == imageUrl
    expect(self.mockUrlSession.receivedRequest?.url?.absoluteString.hasPrefix(imageUrl)) == true
    expect(self.mockUrlSession.mockDataTask.executed) == true
  }

  func testDownloadImage_FileNotFound_CompletionReceivesNilImage() {
    // Given
    mockCache = MockCache(seed: [:])
    store = ImageStore(cache: mockCache, urlSession: mockUrlSession)

    // When
    let pathNotInCache = "path/not/in/cache.jpg"
    let receivedImage = store.image(.w92(path: pathNotInCache)).toBlocking(timeout: 1).firstOrNil()

    // Then
    XCTAssertNil(receivedImage)
  }

  func testDownloadImage_ValidImage_CompletionReceivesImage() {
    // Given
    mockCache = MockCache(seed: [:])
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.imageData(named: "SampleImage"))
    store = ImageStore(cache: mockCache, urlSession: mockUrlSession)

    // When
    let pathNotInCache = "path/not/in/cache.jpg"
    let receivedImage = store.image(.w92(path: pathNotInCache)).toBlocking(timeout: 1).firstOrNil()

    // Then
    XCTAssertNotNil(receivedImage)
  }

  func testDownloadImage_ValidImage_SavesImageToCache() {
    // Given
    mockCache = MockCache(seed: [:])
    mockUrlSession.mockDataTask = MockDataTask(responseData: Stub.imageData(named: "SampleImage"))
    store = ImageStore(cache: mockCache, urlSession: mockUrlSession)

    // When
    let pathNotInCache = "path/not/in/cache.jpg"
    let receivedImage = store.image(.w92(path: pathNotInCache)).toBlocking(timeout: 1).firstOrNil()

    // Then
    let imageUrl = TMDB.image.w92(path: pathNotInCache).url.absoluteString
    let cachedImage = mockCache.object(forKey: imageUrl as NSString)
    XCTAssertEqual(receivedImage, cachedImage)
  }
}
