//
// EventTests.swift
// MoVViesMaster
//
// Created on 7/24/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import XCTest
@testable import Business

// MARK: - Event Tests

final class EventTests: XCTestCase {
  var event: Event<String>!

  override func setUp() {
    super.setUp()
    event = Event<String>()
  }

  override func tearDown() {
    event = nil
    super.tearDown()
  }

  func testAddHandlerIncreaseHandlersCount() {
    // When
    _ = event.add { _ in return }

    // Then
    XCTAssertEqual(event.handlers.count, 1, "Handlers count did not increase")
  }

  func testSingleHandler_RaiseEvent_ShouldRunHandlers() {
    // Given
    var handlerWasCalled = false
    _ = event.add { _ in handlerWasCalled = true }

    // When
    _ = event.raise("Value")

    // Then
    XCTAssert(handlerWasCalled, "Handler was not called")
  }

  func testMultipleHandlers_RaiseEvent_ShouldRunHandlers() {
    // Given
    var handlersCalled = 0

    _ = event.add { _ in handlersCalled += 1 }
    _ = event.add { _ in handlersCalled += 1 }

    // When
    _ = event.raise("Value")

    // Then
    XCTAssertEqual(handlersCalled, 2, "Not all handlers were called")
  }

  func testRaise_ShouldPassAlongTheEventValue() {
    // Given
    let eventValue = "Something happened"
    var receivedValue: String? = nil

    // When
    _ = event.add { value in
      receivedValue = value
    }

    _ = event.raise(eventValue)

    // Then
    XCTAssertEqual(receivedValue, eventValue, "Received value doesn't match event value")
  }
}

// MARK - EventHandlerWrapper Tests

final class EventHandlerWrapperTests: XCTestCase {
  var eventHandler: EventHandlerWrapper<String>!

  override func setUp() {
    super.setUp()
  }

  override func tearDown() {
    eventHandler = nil
    super.tearDown()
  }

  func testWrapper_KeepsReferenceToEvent() {
    // Given
    let event = Event<String>()

    // When
    let handlerWrapper = EventHandlerWrapper(with: { _ in return }, event: event)

    // Then
    XCTAssertEqual(handlerWrapper.event, event)
  }

  func testWrapper_KeepsReferenceToHandler() {
    // Given
    let event = Event<String>()
    var handlerWasCalled = false

    let eventHandler = { (value: String) -> Void in
      handlerWasCalled = true
    }

    // When
    let handlerWrapper = EventHandlerWrapper(with: eventHandler, event: event)

    handlerWrapper.handler("Value")

    // Then
    XCTAssert(handlerWasCalled)
  }

  func testWrapper_Dispose_ReduceEventHandlersCount() {
    // Given
    let event = Event<String>()

    // When
    let disposable = event.add { _ in return }
    disposable.dispose()

    // Then
    XCTAssertEqual(event.handlers.count, 0)
  }

  func testWrapper_KeepsWeakReferenceToEvent() {
    // Given
    var event: Event<String>? = Event<String>()
    let wrapper = EventHandlerWrapper<String>(with: { _ in return }, event: event!)

    // When
    event = nil

    // Then
    guard wrapper.event == nil else {
      XCTFail()
      return
    }
  }

  func testCacheLatestValue_TriggersImmediatelyUponSubscription() {
    // Given
    let event = Event<String>(cacheLatestValue: true)
    let initialValue = "Initial value"
    event.raise(initialValue)
    var receivedValue: String? = nil

    // When
    _ = event.add { value in receivedValue = value }

    // Then
    XCTAssertEqual(receivedValue, initialValue)

    // ...continuation

    // Given
    let newValue = "New value"
    event.raise(newValue)

    // When
    var newReceivedValue: String? = nil
    _ = event.add { value in newReceivedValue = value }

    // Then
    XCTAssertEqual(newReceivedValue, newValue)
  }
}
