//
// URLSession+RxTests.swift
// MoVViesMaster
//
// Created on 8/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxTest
import RxBlocking
import OHHTTPStubs
import Nimble
import RxNimble
@testable import Business

final class URLSessionRxTests: XCTestCase {

  var mockResponseObject = ["key": "value", "foo": "bar"]
  var mockArrayResponseObject: [[String: String]] { return [mockResponseObject] }
  var jsonResponse = URLRequest(url: URL(string: "https://success.com")!)
  var arrayResponse = URLRequest(url: URL(string: "https://array.response.com")!)
  var imageResponse = URLRequest(url: URL(string: "https://image.response.com")!)
  var failureRequest = URLRequest(url: URL(string: "https://failure.com")!)
  var unauthorizedRequest = URLRequest(url: URL(string: "https://unauthorized.com")!)
  var requestWithNonHTTPResponse = URLRequest(url: URL(string: "https://invalid.response.com")!)

  override func setUp() {
    super.setUp()
    stub(condition: isRequest(jsonResponse)) { _ in
      return OHHTTPStubsResponse(jsonObject: self.mockResponseObject, statusCode: 200, headers: nil)
    }

    stub(condition: isRequest(failureRequest)) { _ in
      return OHHTTPStubsResponse(error: RequestError.unknown)
    }

    stub(condition: isRequest(unauthorizedRequest)) { _ in
      return OHHTTPStubsResponse(data: Data(), statusCode: 401, headers: nil)
    }

    stub(condition: isRequest(arrayResponse)) { _ in
      return OHHTTPStubsResponse(jsonObject: self.mockArrayResponseObject, statusCode: 200, headers: nil)
    }

    stub(condition: isRequest(imageResponse)) { _ in
      return OHHTTPStubsResponse(data: Stub.imageData(named: "SampleImage"), statusCode: 200, headers: nil)
    }
  }

  override func tearDown() { super.tearDown() }

  func testRequestDownload_SuccessResponse_SpawnsDownloadedEvent() {
    // Given
    let observable = URLSession.shared.rx.download(request: jsonResponse)

    // When
    let received = observable.toBlocking(timeout: 1).firstOrNil()

    // Then
    expect(received).toNot(be(nil))
  }

  // MARK: - Image

  func testRequestImage_SuccessResponse_SpawnsImageEvent() {
    // Given
    let observable = URLSession.shared.rx.image(request: imageResponse)

    // When
    let received = observable.toBlocking(timeout: 1).firstOrNil()

    // Then
    expect(received).toNot(be(nil))
  }

  func testRequestImage_InvalidResponse_ThrowsError() {
    // Given
    let observable = URLSession.shared.rx.image(request: jsonResponse)

    // When
    let thrownError: ProviderError? = observable.toBlocking(timeout: 1).error()

    // Then
    guard let providerError = thrownError, case ProviderError.invalidResponseData(_) = providerError else {
      fail("Expected error not found: Expected '\(ProviderError.invalidResponseData)', got '\(thrownError)'")
      return
    }
  }
}
