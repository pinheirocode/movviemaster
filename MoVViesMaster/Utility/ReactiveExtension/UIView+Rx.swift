//
// UIView+Rx.swift
// MoVViesMaster
//
// Created on 9/4/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UIView {
  public func alpha(animated: Bool = false) -> UIBindingObserver<Base, CGFloat> {
    return UIBindingObserver(UIElement: self.base) { view, alpha in
      let changes = { view.alpha = alpha }
      if animated {
        UIView.animate(withDuration: 0.3, animations: changes)
      } else {
        changes()
      }
    }
  }
}
