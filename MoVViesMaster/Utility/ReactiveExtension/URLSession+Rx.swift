//
// URLSession+Rx.swift
// MoVViesMaster
//
// Created on 8/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import RxSwift
import RxCocoa

extension Reactive where Base: URLSession {
  public func download(request: URLRequest) -> Observable<(URL)> {
    return Observable.create { observer in
      let task = self.base.downloadTask(with: request) { (fileUrl: URL?, _, error: Error?) -> Void in

        if let error = error {
          observer.onError(ProviderError.unknown(message: error.localizedDescription))
          return
        }

        guard let fileUrl = fileUrl else {
          observer.onError(ProviderError.invalidResponseData(message: "File not found"))
          return
        }

        observer.onNext(fileUrl)
        observer.onCompleted()
      }

      task.resume()

      return Disposables.create(with: task.cancel)
    }
  }

  public func image(request: URLRequest) -> Observable<UIImage> {
    return data(request: request).map { data -> UIImage in
      guard let image = UIImage(data: data) else { throw ProviderError.invalidResponseData(message: "Failed to decode image") }
      return image
    }
  }
}
