//
// Optional+Default.swift
// MoVViesMaster
//
// Created on 8/4/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

extension Optional where Wrapped == String {
  public var orDefault: String {
    switch self {
      case .some(let value): return value
      default: return ""
    }
  }
}
