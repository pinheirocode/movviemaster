//
// Event.swift
// MoVViesMaster
//
// Created on 7/24/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public final class Event<T> {
  private var cacheLatest: Bool
  private var latest: T?

  public typealias Handler = (T) -> Void
  public var handlers: [Handler] {
    return handlerWrappers.map { (wrapper: EventHandlerWrapper<T>) -> Handler in
      return wrapper.handler
    }
  }

  fileprivate var handlerWrappers = [EventHandlerWrapper<T>]()

  init(cacheLatestValue: Bool = false) {
    self.cacheLatest = cacheLatestValue
  }

  public func add(handler: @escaping Handler) -> Disposable {
    let handlerWrapper = EventHandlerWrapper(with: handler, event: self)
    handlerWrappers.append(handlerWrapper)

    if let latest = latest, cacheLatest {
      handler(latest)
    }

    return handlerWrapper
  }

  public func raise(_ value: T) {
    if cacheLatest {
      latest = value
    }
    handlerWrappers.forEach { wrapper in wrapper.handler(value) }
  }

  fileprivate func remove(handler: EventHandlerWrapper<T>) {
    handlerWrappers = handlerWrappers.filter { $0 !== handler }
  }
}

extension Event: Equatable {
  public static func == (lhs: Event<T>, rhs: Event<T>) -> Bool {
    return lhs === rhs
  }
}

final class EventHandlerWrapper<T> {
  typealias EventType = Event<T>

  weak var event: EventType?
  let handler: EventType.Handler

  init(with eventHandler: @escaping EventType.Handler, event: EventType) {
    self.event = event
    self.handler = eventHandler
  }
}

extension EventHandlerWrapper: Disposable {
  func dispose() {
    guard let event = event else { return }
    event.remove(handler: self)
  }
}
