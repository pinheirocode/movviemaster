//
// Disposable.swift
// MoVViesMaster
//
// Created on 7/24/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public protocol Disposable {
  func dispose() -> Void
}
