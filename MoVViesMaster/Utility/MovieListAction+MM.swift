//
// MovieListAction+MM.swift
// MoVViesMaster
//
// Created on 8/7/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import UIKit
import Business

extension MovieListAction {
  var image: UIImage? {
    switch self {
      case .close: return UIImage.mm.closeIcon
      case .search: return UIImage.mm.searchIcon
    }
  }
}
