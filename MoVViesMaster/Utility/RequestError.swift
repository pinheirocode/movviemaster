//
// RequestError.swift
// MoVViesMaster
//
// Created on 8/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

enum RequestError: Error {
  case unauthorized, invalidResponse, unknown
}
