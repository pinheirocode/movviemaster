//
// UIImage+MM.swift
// MoVViesMaster
//
// Created on 8/4/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import UIKit

struct MMImage {
  static let closeIcon = UIImage(named: "close-icon")?.withRenderingMode(.alwaysTemplate)
  static let searchIcon = UIImage(named: "search-icon")?.withRenderingMode(.alwaysTemplate)
}

extension UIImage {
  static var mm: MMImage.Type { return MMImage.self }
}
