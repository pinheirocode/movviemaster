//
// Colors.swift
// MoVViesMaster
//
// Created on 8/2/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import UIKit

struct MMColor {
  static let warmGray = UIColor(red: 0.620, green: 0.580, blue: 0.545, alpha: 1.0)
  static let defaultBackground = UIColor(red: 0.255, green: 0.251, blue: 0.259, alpha: 1.0)
  static let searchTextForeground = UIColor(red: 0.471, green: 0.471, blue: 0.471, alpha: 1.0)
  static let searchBackground = UIColor(red: 0.125, green: 0.125, blue: 0.114, alpha: 1.0)
  static let searchPlaceholderForeground = defaultBackground
}

extension UIColor {
  static var mm: MMColor.Type {
    return MMColor.self
  }
}
