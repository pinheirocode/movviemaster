//
// DynamicProperty.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public final class DynamicProperty<T> {

  public var value: T {
    willSet {
      willChange.raise((value, newValue))
    }
    didSet {
      latestValue.raise(value)
      didChange.raise((oldValue, value))
    }
  }

  public let willChange = Event<(T, T)>()
  public let latestValue = Event<T>(cacheLatestValue: true)
  public let didChange = Event<(T, T)>()

  public init(_ value: T) {
    self.value = value
    latestValue.raise(value)
  }
}
