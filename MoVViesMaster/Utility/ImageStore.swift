//
// ImageStore.swift
// MoVViesMaster
//
// Created on 7/31/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

public typealias ImageResult = UIImage?

public typealias ImageResultHandler = (ImageResult) -> Void

public final class ImageStore: Provider {
  fileprivate var cache: NSCache<NSString, UIImage>
  private var urlSession: URLSession

  private let disposeBag = DisposeBag()

  init(cache: NSCache<NSString, UIImage> = NSCache(), urlSession: URLSession = URLSession.shared) {
    self.cache = cache
    self.urlSession = urlSession
  }

  func image(_ image: TMDBImage) -> Observable<UIImage> {
    let url = image.url
    return object(forKey: image.url.absoluteString as NSString)
      .ifEmpty(
        switchTo: downloadImage(from: url).do(onNext: { [weak self] image in
          self?.cache.setObject(image, forKey: url.absoluteString as NSString)
        })
      )
  }

  private func downloadImage(from url: URL) -> Observable<UIImage> {
    return self.urlSession.rx.image(request: createRequest(url: url))
  }
}

// MARK: - Utility

extension ImageStore {
  func object(forKey key: NSString) -> Observable<UIImage> {
    return Observable.create { observer in
      let disposable = Disposables.create()

      guard let object = self.cache.object(forKey: key) else {
        observer.onCompleted()
        return disposable
      }

      observer.onNext(object)
      observer.onCompleted()
      return disposable
    }
  }
}
