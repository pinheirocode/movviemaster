//
//  AppDelegate.swift
//  MoVViesMaster
//
//  Created by Bruno Pinheiro on 7/24/17.
//  Copyright (c) 2017 ArcTouch. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    UIView.appearance().tintColor = UIColor.mm.warmGray
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.mm.warmGray]

    return true
  }
}
