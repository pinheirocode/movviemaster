//
// MoviesListViewModel.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

public enum MovieListAction {
  case search, close
}

public enum MoviesUpdate {
  case addMovies(Int, at: IndexPath), refreshMovies
}

enum DisplayMode {
  case upcoming, filtered
}

public final class MoviesListViewModel {
  // MARK: - Input
  public let query = Variable<String>("")
  public var currentFilter = Variable<String>("")

  // MARK: - Output
  public let movies = Variable<[Movie]>([])
  public let listUpdates = PublishSubject<MoviesUpdate>()
  public let working = Variable<Bool>(false)
  public let action = Variable<MovieListAction>(.search)
  public let nextPageForUpcoming = Variable<Int>(1)
  public let nextPageForFiltered = Variable<Int>(1)

  public var selectedMovie: Movie? {
    guard let index = selectedIndex.value else {
      return nil
    }

    return movies.value[index]
  }

  // MARK: - Private

  private let upcomingMovies = Variable<[Movie]>([])
  private let filteredMovies = Variable<[Movie]>([])

  private var displayMode = DisplayMode.upcoming

  private let provider: MovieProvider
  private let imageStore: ImageStore

  private let selectedIndex = Variable<Int?>(nil)
  private let disposeBag = DisposeBag()

  public init(provider: MovieProvider, imageStore: ImageStore = ImageStore()) {
    self.provider = provider
    self.imageStore = imageStore

    query.asObservable().subscribe(onNext: { [unowned self] _ in
      self.nextPageForFiltered.value = 1
      self.action.value = .search
    }).disposed(by: disposeBag)

    upcomingMovies.asObservable().subscribe(onNext: { [unowned self] newValue in
      let previousCount: Int = self.movies.value.count
      self.movies.value = newValue

      switch self.displayMode {
        case .upcoming:
          self.listUpdates.onNext(.addMovies(newValue.count - previousCount, at: IndexPath(row: previousCount, section: 0)))
        case .filtered:
          self.displayMode = .upcoming
          self.listUpdates.onNext(.refreshMovies)
      }
    }).disposed(by: disposeBag)

    filteredMovies.asObservable().subscribe(onNext: { [unowned self] newValue in
      let previousCount: Int = self.movies.value.count
      self.movies.value = newValue

      switch self.displayMode {
        case .filtered where !newValue.isEmpty:
          self.listUpdates.onNext(.addMovies(newValue.count - previousCount, at: IndexPath(row: previousCount, section: 0)))
        case .upcoming:
          self.displayMode = .filtered
          self.listUpdates.onNext(.refreshMovies)
        default: break
      }
    }).disposed(by: disposeBag)

    currentFilter.asObservable()
      .distinctUntilChanged { $0.caseInsensitiveCompare($1) == ComparisonResult.orderedSame }
      .subscribe(onNext: { [unowned self] _ in
        self.filteredMovies.value = []
      }).disposed(by: disposeBag)
  }

  public func didTapAction() {
    switch action.value {
      case .close:
        query.value = ""
        loadMovies()
      case .search:
        loadMovies()
    }
  }

  public func didReachListThreshold() {
    loadMovies()
  }

  internal func loadMovies() {
    let filter = query.value
    currentFilter.value = filter
    if filter.isEmpty {
      fetchUpcomingMovies()
    } else {
      fetchFilteredMovies(query: filter)
    }
  }

  public func initializeContent() {
    loadMovies()
  }

  private func fetchFilteredMovies(query: String) {
    working.value = true
    action.value = .close
    provider.rx.fetchMovies(page: nextPageForFiltered.value, query: query)
      .do(
        onNext: { [unowned self] _ in self.nextPageForFiltered.value = self.nextPageForFiltered.value + 1 },
        onDispose: { [unowned self] _ in self.working.value = false }
      )
      .subscribe(onNext: { [unowned self] movies in
        self.filteredMovies.value += movies
      }).disposed(by: disposeBag)
  }

  private func fetchUpcomingMovies() {
    working.value = true
    action.value = .search
    provider.fetchUpcoming(page: nextPageForUpcoming.value) { [weak self] movies, _ in
      guard let strongSelf = self else { return }
      strongSelf.working.value = false
      strongSelf.nextPageForUpcoming.value += 1
      guard let movies = movies else { return }
      strongSelf.upcomingMovies.value += movies
    }
  }

  public func didSelect(index: IndexPath) {
    selectedIndex.value = index.row
  }

  public func fetchThumbnail(for movie: Movie, _ completion: @escaping (UIImage?) -> Void) {
    guard let posterPath = movie.posterPath else {
      completion(nil)
      return
    }

    imageStore.image(.w92(path: posterPath)).subscribe(onNext: { image in
      completion(image)
    }).disposed(by: disposeBag)
  }
}
