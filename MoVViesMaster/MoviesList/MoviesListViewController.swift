//
// MoviesListViewController.swift
// MoVViesMaster
//
// Created on 7/26/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Business

final class MoviesListViewController: UIViewController {
  @IBOutlet fileprivate weak var moviesTableView: UITableView!
  @IBOutlet fileprivate weak var movieSearchTextField: UITextField!
  @IBOutlet fileprivate weak var searchActionView: UIView!
  @IBOutlet fileprivate weak var searchActionImageView: UIImageView!
  @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet fileprivate weak var endMessage: UILabel!

  fileprivate var viewModel: MoviesListViewModel!
  fileprivate var threshold: CGFloat = 0.0

  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    title = "Movies"

    viewModel = MoviesListViewModel(provider: TMDBProvider())

    setupSearch()

    viewModel.query.asDriver().distinctUntilChanged()
      .drive(movieSearchTextField.rx.text)
      .disposed(by: disposeBag)

    viewModel.listUpdates.asObservable()
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [unowned self] (update: MoviesUpdate) in
        switch update {
          case .refreshMovies: self.moviesTableView.reloadData()
          case .addMovies(let count, let startIndex):
            var indexPaths = [IndexPath]()

            for i in 0..<count {
              indexPaths.append(IndexPath(row: i + startIndex.row, section: startIndex.section))
            }
            self.moviesTableView.insertRows(at: indexPaths, with: .fade)
        }
      }).disposed(by: disposeBag)

    let workingDriver = viewModel.working.asDriver()

    workingDriver.drive(activityIndicator.rx.isAnimating).disposed(by: disposeBag)
    workingDriver.map(!).drive(activityIndicator.rx.isHidden).disposed(by: disposeBag)
    workingDriver.map { $0 ? 0.0 : 1.0 }.drive(endMessage.rx.alpha(animated: true)).disposed(by: disposeBag)

    viewModel.initializeContent()
  }

  private func setupSearch() {
    viewModel.action.asDriver().map { $0.image }
      .drive(searchActionImageView.rx.image).disposed(by: disposeBag)

    movieSearchTextField.rx.text.map { $0 ?? "" }
      .distinctUntilChanged().bind(to: viewModel.query).disposed(by: disposeBag)

    guard let placeholder = movieSearchTextField.placeholder else { return }

    let attributedPlaceholder = NSAttributedString(
      string: placeholder,
      attributes: [NSForegroundColorAttributeName: UIColor.mm.searchPlaceholderForeground]
    )
    movieSearchTextField.attributedPlaceholder = attributedPlaceholder

    searchActionView.layer.cornerRadius = searchActionView.frame.height / 2.0
    searchActionView.layer.borderColor = UIColor.mm.warmGray.cgColor
    searchActionView.layer.borderWidth = 2.0
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: true)
  }

  @IBAction func searchActionTapped(_ gesture: UILongPressGestureRecognizer) {
    switch gesture.state {
      case UIGestureRecognizerState.began:
        searchActionView.backgroundColor = UIColor.mm.defaultBackground
      case UIGestureRecognizerState.ended:
        searchActionView.backgroundColor = UIColor.mm.searchBackground
        notifyActionTapped()
      default: break
    }
  }

  fileprivate func showDetails() {
    performSegue(withIdentifier: "showDetail", sender: nil)
  }
}

extension MoviesListViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.movies.value.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "Movie Cell") as? MovieCell else {
      preconditionFailure("A prototype cell with identifier 'Movie Cell' was not found.")
    }

    let movie: Movie = viewModel.movies.value[indexPath.row]
    cell.title = movie.title
    cell.details = movie.overview

    cell.selectedBackgroundView = createSelectedBackgroundView(frame: cell.frame)
    cell.tag = indexPath.row

    viewModel.fetchThumbnail(for: movie) { result in
      DispatchQueue.main.async {
        guard cell.tag == indexPath.row else {
          return
        }

        cell.thumbnail = result
      }
    }

    return cell
  }

  private func createSelectedBackgroundView(frame: CGRect) -> UIView {
    let selectedBackground = UIView(frame: frame)
    selectedBackground.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2)
    return selectedBackground
  }
}

extension MoviesListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    viewModel.didSelect(index: indexPath)
    showDetails()
  }

  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    movieSearchTextField.resignFirstResponder()
  }

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.y > threshold,
       scrollView.contentOffset.y + 2 * scrollView.frame.height >= scrollView.contentSize.height {
      self.threshold = scrollView.contentOffset.y + scrollView.frame.height + 40.0
      viewModel.didReachListThreshold()
    }
  }
}

// MARK: - Search

extension MoviesListViewController {
  func notifyActionTapped() {
    movieSearchTextField.resignFirstResponder()
    viewModel.didTapAction()
  }
}

extension MoviesListViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    notifyActionTapped()
    return false
  }
}

// MARK: - Navigation

extension MoviesListViewController {
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    guard let destination = segue.destination as? MovieDetailViewController,
          let selectedMovie = viewModel.selectedMovie else {
      preconditionFailure("Destination view controller must be a instance of MovieDetailViewController and a movie must be selected")
    }

    destination.viewModel = MovieDetailViewModel(with: selectedMovie)
  }
}
