//
// MovieCell.swift
// MoVViesMaster
//
// Created on 7/26/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import UIKit

final class MovieCell: UITableViewCell {
  @IBOutlet private weak var thumbnailView: UIImageView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var descriptionLabel: UILabel!

  var title: String? {
    get { return titleLabel.text }
    set { titleLabel.text = newValue }
  }

  var details: String? {
    get { return descriptionLabel.text }
    set { descriptionLabel.text = newValue }
  }

  var thumbnail: UIImage? {
    get { return thumbnailView.image }
    set {
      guard let newValue = newValue else {
        thumbnailView.contentMode = .scaleAspectFit
        thumbnailView.image = UIImage(named: "film")?.withRenderingMode(.alwaysTemplate)
        return
      }

      thumbnailView.contentMode = .scaleAspectFill
      UIView.transition(with: thumbnailView, duration: 0.3, options: .transitionCrossDissolve, animations: {
        self.thumbnailView.image = newValue
      }, completion: nil)
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    title = nil
    details = nil
    thumbnail = nil
  }
}
