//
// TMDBProvider.swift
// MoVViesMaster
//
// Created on 7/26/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public final class TMDBProvider: MovieProvider {

  private var urlSession: URLSession

  public init(urlSession: URLSession = URLSession.shared) {
    self.urlSession = urlSession
  }

  public func fetchMovies(page: Int = 1, query: String, completion: @escaping FetchResultHandler) {
    guard !query.isEmpty else {
      completion(nil, ProviderError.invalidRequestData(message: "Query string must not be empty"))
      return
    }

    let request = createRequest(url: TMDB.searchMovies.url, queryParams: [
      "page": String(page),
      "query": query
    ])

    let dataTask = urlSession.dataTask(with: request) { [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
      self?.processMoviesResponse(data, response: response, error: error, completion: completion)
    }

    dataTask.resume()
  }

  private func processMoviesResponse(_ data: Data?, response: URLResponse?, error: Error?, completion: ([Movie]?, ProviderError?) -> Void) {
    if case .fail(let error) = checkRequestStatus(response: response, error: error) {
      completion(nil, error)
      return
    }

    do {
      let movies: [Movie] = try extract(from: data, path: "results")
      completion(movies, nil)
    } catch {
      completion(nil, .invalidResponseData(message: "Response data did not contain a valid json"))
    }
  }

  public func fetchUpcoming(page: Int = 1, completion: @escaping FetchResultHandler) {
    let request = createRequest(url: TMDB.upcomingMovies.url, queryParams: ["page": String(page)])

    let task = urlSession.dataTask(with: request) { [weak self] data, response, error in
      guard let strongSelf = self else { return }

      if case .fail(let error) = strongSelf.checkRequestStatus(response: response, error: error) {
        completion(nil, error)
        return
      }

      do {
        let movies: [Movie] = try strongSelf.extract(from: data, path: "results")
        completion(movies, nil)
      } catch {
        completion(nil, .invalidResponseData(message: "Response data did not contain a valid json"))
      }
    }

    task.resume()
  }

  private func extract<T:AutoBuild>(from data: Data?, path: String) throws -> [T] {
    guard let data = data,
          let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any],
          let infoArray = jsonObject[path] as? [[String: Any]] else {
      return []
    }

    let movies = infoArray.map { T.init(with: $0) }
    return movies
  }
}

// MARK: - Validation

fileprivate enum RequestStatus {
  case ok, fail(error: ProviderError)
}

extension TMDBProvider {
  fileprivate func checkRequestStatus(response: URLResponse?, error: Error?) -> RequestStatus {
    guard error == nil, let httpResponse = response as? HTTPURLResponse else {
      return .fail(error: .unknown(message: "Operation failed."))
    }

    switch httpResponse.statusCode {
      case 200: return .ok
      case 401: return .fail(error: .accessDenied(message: "API Key is invalid."))
      default: return .fail(error: .unknown(message: "Operation failed."))
    }
  }
}
