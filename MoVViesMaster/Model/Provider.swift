//
// Provider.swift
// MoVViesMaster
//
// Created on 7/31/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public protocol Provider {}

// MARK: - Utility

extension Provider {

  public func createRequest(url: URL, queryParams: [String: String] = [:]) -> URLRequest {
    guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
      fatalError("Malformed URL")
    }

    let defaultParams = [
      "api_key": "1f54bd990f1cdfb230adb312546d765d",
      "language": "en-US"
    ]

    var queryItems = [URLQueryItem]()
    queryItems.append(contentsOf: defaultParams.map { return URLQueryItem(name: $0, value: $1) })
    queryItems.append(contentsOf: queryParams.map { return URLQueryItem(name: $0, value: $1) })

    components.queryItems = queryItems

    return URLRequest(url: components.url!)
  }
}
