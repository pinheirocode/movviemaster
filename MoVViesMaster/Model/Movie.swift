//
// Movie.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public struct Movie {
  public var voteCount: Int?
  public var id: Int?
  public var video: Bool?
  public var voteAverage: Double?
  public var title: String?
  public var popularity: Double?
  public var posterPath: String?
  public var originalLanguage: String?
  public var originalTitle: String?
  public var genreIds: [Int]?
  public var backdropPath: String?
  public var adult: Bool?
  public var overview: String?
  public var releaseDate: Date?
}

extension Movie: AutoBuild {
  public init(with info: [String: Any]) {
    self.voteCount = info["vote_count"] as? Int
    self.id = info["id"] as? Int
    self.video = info["video"] as? Bool
    self.voteAverage = info["vote_average"] as? Double
    self.title = info["title"] as? String
    self.popularity = info["popularity"] as? Double
    self.posterPath = info["poster_path"] as? String
    self.originalLanguage = info["original_language"] as? String
    self.originalTitle = info["original_title"] as? String
    self.genreIds = info["genre_ids"] as? [Int]
    self.backdropPath = info["backdrop_path"] as? String
    self.adult = info["adult"] as? Bool
    self.overview = info["overview"] as? String
    if let releaseDate = info["release_date"] as? String {
      self.releaseDate = parseDate(releaseDate)
    }
  }

  internal func parseDate(_ dateString: String) -> Date? {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return formatter.date(from: dateString)
  }
}

extension Movie: Equatable {
  public static func == (lhs: Movie, rhs: Movie) -> Bool {
    return lhs.id == rhs.id || lhs.originalTitle == rhs.originalTitle
  }
}
