//
// AutoBuild.swift
// MoVViesMaster
//
// Created on 7/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public protocol AutoBuild {
  init(with: [String: Any])
}
