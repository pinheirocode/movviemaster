//
// MoviesProvider.swift
// MoVViesMaster
//
// Created on 7/25/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import RxSwift

public typealias FetchResultHandler = ([Movie]?, ProviderError?) -> Void

public protocol MovieProvider: Provider {
  func fetchMovies(page: Int, query: String, completion: @escaping FetchResultHandler)
  func fetchUpcoming(page: Int, completion: @escaping FetchResultHandler)
}
