//
// RequestError.swift
// MoVViesMaster
//
// Created on 7/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public enum ProviderError: Error {
  case accessDenied(message: String)
  case requestTimeout(message: String)
  case invalidRequestData(message: String)
  case invalidResponseData(message: String)
  case unknown(message: String?)
}
