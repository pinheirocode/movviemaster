//
// MovieProvider+Rx.swift
// MoVViesMaster
//
// Created on 8/28/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import RxSwift

public struct RxMovieProvider {
  private var provider: MovieProvider

  init(_ provider: MovieProvider) {
    self.provider = provider
  }

  func fetchMovies(page: Int, query: String) -> Observable<[Movie]> {
    return Observable.create { observer in
      self.provider.fetchMovies(page: page, query: query) { movies, error in
        if let error = error { observer.onError(error); return }
        observer.onNext(movies ?? [])
        observer.onCompleted()
      }

      return Disposables.create()
    }
  }

  func fetchUpcoming(page: Int, completion: @escaping FetchResultHandler) {

  }
}

public extension MovieProvider {
  public var rx: RxMovieProvider {
    return RxMovieProvider(self)
  }

}
