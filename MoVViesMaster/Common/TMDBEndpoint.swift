//
// TMDBEndpoint.swift
// MoVViesMaster
//
// Created on 7/27/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public enum TMDB {
  case upcomingMovies, searchMovies
}

extension TMDB: Endpoint {
  public var baseUrl: URL { return URL(string: "https://api.themoviedb.org/3")! }
}

extension TMDB: RequestComponents {
  public var url: URL {
    switch self {
      case .upcomingMovies: return baseUrl.appendingPathComponent("/movie/upcoming")
      case .searchMovies: return baseUrl.appendingPathComponent("/search/movie")
    }
  }
  public var httpMethod: String {
    switch self {
      case .upcomingMovies,
           .searchMovies(_):
        return "GET"
    }
  }
}

public enum TMDBImage {
  case w92(path: String), w780(path: String)
}

extension TMDBImage: Endpoint {
  public var baseUrl: URL { return URL(string: "https://image.tmdb.org/t/p")! }
}

extension TMDBImage: RequestComponents {
  public var url: URL {
    switch self {
      case .w92(let path): return baseUrl.appendingPathComponent("/w92/\(path)")
      case .w780(let path): return baseUrl.appendingPathComponent("/w780/\(path)")
    }
  }
  public var httpMethod: String {
    switch self {
      case .w92(_),
           .w780(_):
        return "GET"
    }
  }
}

extension TMDB {
  static var image: TMDBImage.Type { return TMDBImage.self }
}
