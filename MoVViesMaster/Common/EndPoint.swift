//
// EndPoint.swift
// MoVViesMaster
//
// Created on 7/27/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation

public protocol Endpoint {
  var baseUrl: URL { get }
}

public protocol RequestComponents {
  var url: URL { get }
  var httpMethod: String { get }
}
