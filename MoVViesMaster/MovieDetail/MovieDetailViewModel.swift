//
// MovieDetailViewModel.swift
// MoVViesMaster
//
// Created on 8/2/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

public final class MovieDetailViewModel {

  public let title = Variable<String>("")
  public let overview = Variable<String>("")
  public let backdropImage = Variable<UIImage?>(nil)

  // MARK: - Private

  private let movie: Movie
  private let imageStore: ImageStore
  private let disposeBag = DisposeBag()

  public init(with movie: Movie, imageStore: ImageStore = ImageStore()) {
    self.movie = movie
    self.imageStore = imageStore
    title.value = movie.title.orDefault
    overview.value = movie.overview.orDefault
  }

  public func loadDetails() {
    guard let path = movie.backdropPath else { return }

    imageStore.image(.w780(path: path)).subscribe(onNext: { [unowned self] result in
      self.backdropImage.value = result
    }).disposed(by: disposeBag)
  }
}
