//
// MovieDetailViewController.swift
// MoVViesMaster
//
// Created on 8/1/17.
// Copyright (c) 2017 ArcTouch. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Business

final class MovieDetailViewController: UIViewController {
  @IBOutlet private var backdropImageView: UIImageView!
  @IBOutlet private var overviewLabel: UILabel!

  var viewModel: MovieDetailViewModel!

  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.setNavigationBarHidden(false, animated: true)
    title = viewModel.title.value
    overviewLabel.text = viewModel.overview.value

    viewModel.backdropImage.asDriver().skip(1)
      .drive(backdropImageView.rx.image(transitionType: kCATransitionFade))
      .disposed(by: disposeBag)

    viewModel.loadDetails()
  }
}
